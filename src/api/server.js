"use strict";
const
    environment = process.env.NODE_ENV || 'development',
    log = require('npmlog'),
    config = require('./config/' + environment + '.json'),
    port = process.env.NODE_PORT || 3000,
    logger = require('morgan'),
    express = require('express'),
    cors = require('express-cors'),
    bodyParser = require('body-parser'),
    app = express(),
    router = require('./routes')(express, config),
    db = require('./infra/db.js')(config, log),
    Models = require('./models');

app.use(cors({
    allowedOrigins: config.allowedOrigins,
    headers: ['X-Requested-With', 'Content-Type', 'Authorization']
}));

app.use(bodyParser.json());
app.use(logger('combined'));
app.set('port', port);

app.use('/', router);


app.listen(app.get('port'), function () {
    log.info('SERVER', 'Express server listening on port ' + app.get('port'));
    log.info('SERVER', environment + ' mode.');
});

process.once('SIGTERM', function (sig) {
    queue.shutdown(function (err) {
        console.log('Kue is shut down.', err || '');
        process.exit(0);
    }, 5000);
});

module.exports = app;