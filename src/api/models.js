'use strict';
/*
 * Namespace com os models da aplicacao
 *
 *
 * Copyright (c) 2014 lcardoso
 * Licensed under the MIT license.
 */

const
    mongoose = require('mongoose'),
    FS = require('fs'),
    Path = require('path'),
    changeCase = require('change-case');

let dir = Path.resolve(__dirname, './domain'),
    files = FS.readdirSync(dir),
    map = {};

for (let i = 0; i < files.length; i++) {
    let file = files[i];
    let ext = Path.extname(file);
    let base = changeCase.pascalCase(Path.basename(file, ext));
    let path = Path.resolve(dir, file);
    map[base] = require(path);
};

module.exports = map;