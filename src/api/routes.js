'use strict';
/*
 * Routas da API
 *
 *
 * Copyright (c) 2014 lcardoso
 * Licensed under the MIT license.
 */
module.exports = function (express, config) {
    const
        router = express.Router(),
        FS = require('fs'),
        Path = require('path');

    let dir = Path.resolve(__dirname, './routes'),
        files = FS.readdirSync(dir);

    for (let i = 0; i < files.length; i++) {
        let file = files[i];
        let path = Path.resolve(dir, file);

        require(path)(router, config);
    }

    return router;
}