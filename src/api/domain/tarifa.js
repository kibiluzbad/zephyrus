"use strict";
const
    mongoose = require('mongoose');

/*
 * Schema de Tarifa
 *
 * @class
 * Copyright (c) 2014 kibiluzbad
 * Licensed under the MIT license.
 */
let TarifaSchema = mongoose.Schema({
	dddOrigem: {
        type: String,
        required: true
    },
    dddDestino: {
        type: String,
        required: true
    },
    valorDoMinuto: {
        type: Number,
        required: true
    },

});

module.exports = TarifaSchema;