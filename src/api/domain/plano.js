"use strict";
const
    mongoose = require('mongoose'),
    Tarifa = require('./tarifa.js'),
    Q = require('q'),
    check = require('check-types');

/*
 * Schema de Plano
 *
 * @class
 * Copyright (c) 2014 kibiluzbad
 * Licensed under the MIT license.
 */
let PlanoSchema = mongoose.Schema({
    nome: {
        type: String,
        required: true,
        unique: true
    },
    minutos: {
        type: Number,
        required: true
    },
    percentualAcrescimoMinutosExcedentes: {
        type: Number,
        required: true
    },
    tarifas: [Tarifa]
});

/**
 * Calcula valor do minuto da chamda
 * @param {String} Ddd de origem
 * @param {String} Ddd de destino
 * @return {Number} Valor do minuto
 */
PlanoSchema.methods.valorDoMinuto = function (dddOrigem, dddDestino) {
    if (!dddOrigem || !check.string(dddOrigem)) throw new Error('O ddd de origem deve ser informado');
    if (!dddDestino || !check.string(dddDestino)) throw new Error('O ddd de destino deve ser informado');

    let defered = Q.defer(),
        self = this;
    self.tarifas.forEach(function(tarifa, index){
        if(tarifa.dddOrigem === dddOrigem && tarifa.dddDestino === dddDestino){
            defered.resolve(tarifa.valorDoMinuto);
        }else if(index === self.tarifas.length -1){
            defered.reject('No match');
        }
    });

    return defered.promise;
};

/**
 * Calcula o valor do minuto extra
 * @param {Number} Valor do minuto sem acrescimo
 * @return {Number} Valor do minuto com acrescimo
 */
PlanoSchema.methods.calculaAcrescimo =function(valorDoMinuto){
    if (!check.number(valorDoMinuto)) throw new Error('Informe o valor do minuto.');
    let self = this;
    let acrescimo = valorDoMinuto * self.percentualAcrescimoMinutosExcedentes / 100;

    return valorDoMinuto + acrescimo;
}

/**
 * Calcula as tarifas da chamada com e se o plano.
 * @param {String} Ddd de origem
 * @param {String} Ddd de destino
 * @param {Number} Tempo total da chamda em minutos
 * @return {Number} sum
 */
PlanoSchema.methods.calculaTarifas = function (dddOrigem, dddDestino, minutos) {
    if (!dddOrigem || !check.string(dddOrigem)) throw new Error('O ddd de origem deve ser informado');
    if (!dddDestino || !check.string(dddDestino)) throw new Error('O ddd de destino deve ser informado');
    if (!check.number(minutos)) throw new Error('Informe o tempo em minutos.');

    let defered = Q.defer(),
        self = this;

    self.valorDoMinuto(dddOrigem,dddDestino)
        .then(function(valorDoMinuto){
            let extras = (minutos - self.minutos);
            let comPlano =(0 > extras) ? 0 : self.calculaAcrescimo(valorDoMinuto) * extras;
            let semPlano = minutos * valorDoMinuto;
            let result = {
                comPlano: comPlano,
                semPlano: semPlano
            };
            defered.resolve(result);
        }).catch(function(){
            defered.reject('No match');
        });

    return defered.promise;
};


module.exports = mongoose.models.Plano || mongoose.model('Plano', PlanoSchema);