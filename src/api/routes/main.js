'use strict';
/*
 * Rota principal
 *
 *
 * Copyright (c) 2014 lcardoso
 * Licensed under the MIT license.
 */

const Models = require("../models.js");

module.exports = function (router, config) {

    /*
     * Rota principal da api
     *
     * GET /
     *
     * Copyright (c) 2014 kibiluzbad
     * Licensed under the MIT license.
     */
    router.get('/', function (req, res) {
        let pkg = require("../package.json");
        res.json({
            version: pkg.version,
            name: pkg.name,
            message: 'I\'m alive!'
        });
    });

    /*
     * Rota de setup do banco
     *
     * GET /setup
     *
     * Copyright (c) 2014 kibiluzbad
     * Licensed under the MIT license.
     */
    router.get('/setup', function (req, res) {
        if (process.env.NODE_ENV === 'production') {
            return res.status(404);
        }

        let tarifas = new Array();
        tarifas.push({dddOrigem: '011', dddDestino: '016', valorDoMinuto: 1.90});
        tarifas.push({dddOrigem: '016', dddDestino: '011', valorDoMinuto: 2.90});
        tarifas.push({dddOrigem: '011', dddDestino: '017', valorDoMinuto: 1.70});
        tarifas.push({dddOrigem: '017', dddDestino: '011', valorDoMinuto: 2.70});
        tarifas.push({dddOrigem: '011', dddDestino: '018', valorDoMinuto: 0.90});
        tarifas.push({dddOrigem: '018', dddDestino: '011', valorDoMinuto: 1.90});
        let plano30 = new Models.Plano({nome:'FaleMais30', minutos: 30, percentualAcrescimoMinutosExcedentes: 10, tarifas: tarifas}),
        plano60 = new Models.Plano({nome:'FaleMais60', minutos: 60, percentualAcrescimoMinutosExcedentes: 10, tarifas: tarifas}),
        plano120 = new Models.Plano({nome:'FaleMais120', minutos: 120, percentualAcrescimoMinutosExcedentes: 10, tarifas: tarifas});

        plano30.save(function(err){
            if(err) return res.status(500).json({message: err.message});
            plano60.save(function(err){
                if(err) return res.status(500).json({message: err.message});
                plano120.save(function(err){
                    if(err) return res.status(500).json({message: err.message});
                    res.status(201).json({message: 'Done'});
                });
            });
        });
    });

    return router;
};