'use strict';
/*
 * Rota principal
 *
 *
 * Copyright (c) 2014 lcardoso
 * Licensed under the MIT license.
 */

const Models = require("../models.js"),
    check = require('check-types');

module.exports = function (router, config) {

    /*
     * Retorna as tarifas de um plano
     *
     * GET /tarifa/5448466e2ae8dff24125dc04?dddOrigem=013&dddDestino=016&minutos=20
     *
     * Copyright (c) 2014 kibiluzbad
     * Licensed under the MIT license.
     */
    router.get('/tarifa/:id', function (req, res) {
        let dddOrigem = req.query.dddOrigem,
            dddDestino = req.query.dddDestino,
            minutos= parseInt(req.query.minutos);

        if(!dddOrigem || !check.string(dddOrigem) ||
            !dddDestino || !check.string(dddDestino) ||
            !check.number(minutos)) return res.status(400).json({message: 'Ivalida data, should inform: dddOrigem, dddDestino, minutos'});

        Models.Plano.findById(req.params.id)
            .exec(function(err, data){
                if(err) return res.status(500).json({message: err.message});
                if(!data) return res.status(404).json({message: 'Not found'});
                 data.calculaTarifas(dddOrigem,dddDestino,minutos)
                     .then(function(data){
                       res.json(data);
                     })
                     .catch(function(err){
                         if(err === 'No match') return res.status(404).json({message: 'Not found'});
                         res.status(500).json({message: err.message});
                     })
                     .done();
            });
    });

    /*
     * Retorna os planos disponiveis
     *
     * GET /planos
     *
     * Copyright (c) 2014 kibiluzbad
     * Licensed under the MIT license.
     */
    router.get('/planos', function (req, res) {
        Models.Plano.find()
        .select('nome minutos _id')
        .exec(function(err, data){
                if(err) return res.status(500).json({message: err.message});
                if(!data) return res.status(404).json({message: 'Not found'});
                res.json(data);
            });
    });


    return router;
};