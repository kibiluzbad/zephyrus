'use strict';

const mongoose = require('mongoose'),
    config = require('../../config/test.json'),
    fakeLog = {
        error: function (text) {},
        info: function (text) {}
    },
    db = require('../../infra/db.js')(config, fakeLog),
    Plano = require('../../domain/plano.js');

const should = require('should');

describe('Plano', function () {

    let plano30 = {},
        plano60= {},
        plano120= {};

    beforeEach(function (done) {
        Plano.remove(function () {
            let tarifas = new Array();
            tarifas.push({dddOrigem: '011', dddDestino: '016', valorDoMinuto: 1.90});
            tarifas.push({dddOrigem: '016', dddDestino: '011', valorDoMinuto: 2.90});
            tarifas.push({dddOrigem: '011', dddDestino: '017', valorDoMinuto: 1.70});
            tarifas.push({dddOrigem: '017', dddDestino: '011', valorDoMinuto: 2.70});
            tarifas.push({dddOrigem: '011', dddDestino: '018', valorDoMinuto: 0.90});
            tarifas.push({dddOrigem: '018', dddDestino: '011', valorDoMinuto: 1.90});
            plano30 = new Plano({nome:'FaleMais30', minutos: 30, percentualAcrescimoMinutosExcedentes: 10, tarifas: tarifas});
            plano60 = new Plano({nome:'FaleMais60', minutos: 60, percentualAcrescimoMinutosExcedentes: 10, tarifas: tarifas});
            plano120 = new Plano({nome:'FaleMais120', minutos: 120, percentualAcrescimoMinutosExcedentes: 10, tarifas: tarifas});
            plano30.save(function(){
                plano60.save(function(){
                    plano120.save(function(){
                        done();
                    });
                })
            });

        });
    });


    describe('Calcular tarifas', function () {

        it('chamadas de 20 minutos de 011 para 016 no FaleMais 30 devem retornar tarifa de $0,00 com plano e $38,00 sem plano.', function (done) {

            let origem= '011',
                destino= '016',
                minutos= 20;

            plano30.calculaTarifas(origem,destino,minutos).then(function(tarifas){
                tarifas.comPlano.should.be.exactly(0.00);
                tarifas.semPlano.should.be.exactly(38.00);
            }).done(done);

        });

        it('chamadas de 80 minutos de 011 para 017 no FaleMais 60 devem retornar tarifa de $37,40 com plano e $136,00 sem plano.', function (done) {

            let origem= '011',
                destino= '017',
                minutos= 80;

            plano60.calculaTarifas(origem,destino,minutos).then(function(tarifas){
                tarifas.comPlano.should.be.exactly(37.40);
                tarifas.semPlano.should.be.exactly(136.00);

            }).done(done);

        });

        it('chamadas de 200 minutos de 018 para 011 no FaleMais 120 devem retornar tarifa de $167,20 com plano e $380,00 sem plano.', function (done) {

            let origem= '018',
                destino= '011',
                minutos= 200;

            plano120.calculaTarifas(origem,destino,minutos).then(function(tarifas){

                tarifas.comPlano.should.be.exactly(167.20);
                tarifas.semPlano.should.be.exactly(380.00);

            }).done(done);

        });

        it('chamadas para origem e destino nao mapeados na tabela devem retornar um objeto undefined.', function (done) {

            let origem= '018',
                destino= '017',
                minutos= 100;

            plano30.calculaTarifas(origem,destino,minutos).then(function(tarifas){
                throw new Error('Should not be here');
            }).catch(function(err){
                console.log(err);
                (err).should.be.exactly('No match');

            }).done(done);

        });

    });

});