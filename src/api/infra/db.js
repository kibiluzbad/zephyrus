'use strict';

const mongoose = require('mongoose');

module.exports = function (config, log) {

    let db = mongoose.connection;
    if (1 !== db.readyState) mongoose.connect(config.db);

    mongoose.connection.on('connected', function () {
        log.info('mongodb', 'Connected succefully to ' + config.db);
    });

    mongoose.connection.on('error', function (err) {
        log.error('mongodb', err);
    });

    mongoose.connection.on('disconnected', function () {
        log.info('mongodb', 'Connected from ' + config.db);
    });

    process.on('SIGINT', function () {
        mongoose.connection.close(function () {
            log.info('Mongoose default connection disconnected through app termination');
            process.exit(0);
        });
    });

    return mongoose;
}
