'use strict';

/**
 * @ngdoc service
 * @name appApp.plano
 * @description
 * # plano
 * Provider in the appApp.
 */
angular.module('appApp')
  .provider('Plano', function () {

        this.$get = ['$resource', 'configuration',
            function ($resource, configuration) {
                var Plano = $resource(configuration.apiUrl + '/planos',{
                    id: '@id'
                },{
                    calculaTarifa: {
                        method: 'GET',
                        url: configuration.apiUrl + '/tarifa/:id'
                    }
                });
                return Plano;
            }];
  });
