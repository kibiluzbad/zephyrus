'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:CalculadoraCtrl
 * @description
 * # CalculadoraCtrl
 * Controller of the appApp
 */
angular.module('appApp')
    .controller('CalculadoraCtrl', ['$scope', 'Plano', function ($scope, Plano) {
        $scope.planos = Plano.query();
        $scope.origem = '';
        $scope.destino= '';
        $scope.minutos= '';
        $scope.plano = '';
        $scope.ddds = ['011','016','017','018'];

        $scope.enviar = function(){
            $scope.info = false;
            $scope.error = false;
            $scope.result = false;

            Plano.calculaTarifa({
                id: $scope.plano,
                dddOrigem: $scope.origem,
                dddDestino: $scope.destino,
                minutos: $scope.minutos,
            },function(data){
                $scope.result = data;
            }, function(error){
                console.log(error);
                $scope.result = false;
                if(404 == error.status)
                    $scope.info = "Parece que ainda não estamos atendendo a origem e destino informados! :(";
                else
                    $scope.error = error.data.message;
            })
        };
    }]);
