'use strict';

/**
 * @ngdoc directive
 * @name appApp.directive:btnloading
 * @description
 * # btnloading
 */
angular.module('appApp')
    .directive("btnLoading", function(){
        return{
            link: function(scope, element, attrs){
                element.bind("click", function (event) {
                    element.button("loading");
                });
            },
            restrict: 'A'
        };
  });
