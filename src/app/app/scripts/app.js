'use strict';

/**
 * @ngdoc overview
 * @name appApp
 * @description
 * # appApp
 *
 * Main module of the application.
 */
angular
    .module('appApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'services.config',
        'ui.bootstrap',
    ])
    .config(['$routeProvider', '$httpProvider',function ($routeProvider, $httpProvider) {

        $httpProvider.defaults.withCredentials = true;

        $httpProvider.interceptors.push(['$q',
            function ($q) {
                return {
                    'response': function (response) {
                        $('.btn').button('reset');
                        return response || $q.when(response);
                    },

                    'responseError': function (rejection) {
                        $('.btn').button('reset');
                        return $q.reject(rejection);
                    }
                };
            }]);

        $routeProvider
            .when('/planos', {
                templateUrl: 'views/planos.html',
                controller: 'PlanosCtrl'
            })
            .otherwise({
                redirectTo: '/planos'
            });
    }]);
