'use strict';

describe('Service: plano', function () {

  // load the service's module
  beforeEach(module('appApp'));

  // instantiate service
  var plano;
  beforeEach(inject(function (_plano_) {
    plano = _plano_;
  }));

  it('should do something', function () {
    expect(!!plano).toBe(true);
  });

});
