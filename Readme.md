[ ![Codeship Status for kibiluzbad/zephyrus](https://www.codeship.io/projects/ca321420-3d2c-0132-78d8-524bbe6961f6/status)](https://www.codeship.io/projects/43215)

# Setup

## Instalacao do Mongo DB

Instale uma versao do Mongo do site deles:
[http://www.mongodb.org/downloads](http://www.mongodb.org/downloads)

## Instalacao do Node JS

Instale o Node Js, preferencialmente via nvm:

[https://github.com/creationix/nvm](https://github.com/creationix/nvm)

## Configuracao do ambiente de desenvolvimento

### Instale o yeoman

    $> npm install -g yo

### Instale o grunt

    $> npm install -g grunt-cli

### Instale o gerador do angurlar para yeoman

    $> npm install -g generator-angular


### Instale o bower

    $> npm install -g bower

### Instale o mocha

    $> npm install -g mocha

# Rodando a aplicacao

## Baixando as dependencias

### API

Vá para <raiz da apalicacao>/src/api e instale as dependencias:

    $> cd /src/api
    $> npm install

### SPA

Vá para <raiz da apalicacao>/src/app  e instale as dependencias:

    $> cd /src/app
    $> npm install
    $> bower install

## Iniciando a API

Vá para <raiz da apalicacao>/src/api e inicie o servidor da API:

    $> cd /src/api
    $> npm start

Abrir no browser o seguinte endereco:

[http://localhost:3000](http://localhost:3000)

Um json com o versao e o nome api deve ser exibido.

Executar a rota de setup para criar os dados iniciais:

    $> curl -i http://localhost:3000/setup

Se tudo estiver correto uma mensagem com o texto "Done" deve ser exibida.

## Iniciando a SPA

Vá para <raiz da apalicacao>/src/app e inicie o servidor da SPA:

    $> cd /src/app
    $> grunt serve


# Executando os testes da API

Vá para <raiz da apalicacao>/src/api e execute os testes via npm (o mocha deve estar instalado)

    $> cd /src/api
    $> npm test